$(function() {
	var gallery_no = 0;
	var x_position = 0;
	var y_position = 0;
	get_arr_no = function (no) {
		return 8 - ((no % 8) * -1) - 8;
	}
	$(".gallery_3d dd").eq(get_arr_no(gallery_no)).css({'opacity':'1'});

	move_position = function (sort) {
		switch(sort) {
			case "prev":
				gallery_no -= 1;
				break;

			case "next":
				gallery_no += 1;
				break;
		}

		arr_no = get_arr_no(gallery_no);
		y_position = gallery_no * (360 / $(".gallery_3d dd").length) * -1;
		$(".gallery_3d").css({'transform':'rotateX(' + x_position + 'deg) rotateY(' + y_position + 'deg)'});
		$(".gallery_3d dd").stop().animate({'opacity':'0.75'},{duration:300});
		$(".gallery_3d dd").eq(arr_no).stop().animate({'opacity':'1'},{duration:300});
	}

	displaywheel = function (e){
		var evt = window.event || e //equalize event object
		var delta = evt.detail? evt.detail*(-120) : evt.wheelDelta //check for detail first so Opera uses that instead of wheelDelta
		switch(delta) {
			case 120:
				move_position('prev');
				break;

			case -120:
				move_position('next');
				break;
		}
	}

	var mousewheelevt = (/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x

	if (document.attachEvent) //if IE (and Opera depending on user setting)
		document.attachEvent("on"+mousewheelevt, displaywheel)
	else if (document.addEventListener) //WC3 browsers
		document.addEventListener(mousewheelevt, displaywheel, false)

	$(window).keydown(function(event) {
		switch(event.keyCode) {
			// UP
			case 38:
				x_position += 5;
				$(".gallery_3d").css({'transform':'rotateX(' + x_position + 'deg) rotateY(' + y_position + 'deg)'});
				break;

			// DOWN
			case 40:
				x_position -= 5;
				$(".gallery_3d").css({'transform':'rotateX(' + x_position + 'deg) rotateY(' + y_position + 'deg)'});
				break;

			// PREV
			case 37:
				move_position('prev');
				break;

			// NEXT
			case 39:
				move_position('next');
				break;
		}
	});
});
$(function() {
	$("#letter_contents").blur(function() {
		if(getCookie('letter_contents') != $(this).val() && confirm('저장하시겠습니까?\r\n저장하지 않으시면 수정하신 내용이 초기화 됩니다.')) {
			setCookie('letter_contents', $(this).val(), 9999);
		}
		else {
			$("#letter_contents").val(getCookie('letter_contents'));
		}
	});

	$("#letter_contents").val(getCookie('letter_contents'));

	$(".front a.btn_reverse").click(function() {
		$(".letter_card").css({
			'animation-name':'card_back',
			'animation-iteration-count':'1',
			'animation-delay':'0s',
			'animation-duration':'0.5s',
			'animation-fill-mode':'forwards',
			'animation-play-state':'running',
			'animation-timing-function':'linear'
		});
	});

	$(".back a.btn_reverse").click(function() {
		$(".letter_card").css({
			'animation-name':'card_front',
			'animation-iteration-count':'1',
			'animation-delay':'0s',
			'animation-duration':'0.5s',
			'animation-fill-mode':'forwards',
			'animation-play-state':'running',
			'animation-timing-function':'linear'
		});
	});
});

$(function() {
	var cube_object = document.querySelector(".btn_cube");
	var cube_mouse_pos;
	var start_pos;

	get_position = function(event) {
		cube_mouse_pos = [(event.pageX - cube_object.offsetLeft), (event.pageY - cube_object.offsetTop)];
		do_cube_rolling();
		document.querySelector("#pos").value = cube_mouse_pos;
	}

	do_cube_rolling = function(event) {
		//cube_object.style.transform = "rotateX(" + rotationX + "deg) rotateY(" + rotationY + "deg)";
		y_pos = cube_mouse_pos[0];
		x_pos = cube_mouse_pos[1] * -1;
		cube_object.style.transform = "rotateX(" + x_pos + "deg) rotateY(" + y_pos + "deg)";
	}

	addEventListeners(cube_object,"mousedown", function() {
		var matrix = new WebKitCSSMatrix(window.getComputedStyle(cube_object, null).getPropertyValue("transform"));
		pi = Math.PI;
		var rotationX = Math.round(Math.acos(matrix.a) * (180/pi));
		var rotationY = Math.round(Math.asin(matrix.b) * (180/pi));
		console.log(matrix);
		console.log(Math.round(Math.acos(matrix.a) * (180/pi)));
		console.log(Math.round(Math.acos(matrix.b) * (180/pi)));
		console.log(Math.round(Math.acos(matrix.m12) * (180/pi)));
		console.log(Math.round(Math.acos(matrix.m13) * (180/pi)));
		console.log(Math.round(Math.acos(matrix.m22) * (180/pi)));
		console.log(Math.round(Math.acos(matrix.m23) * (180/pi)));
		console.log(Math.round(Math.acos(matrix.m32) * (180/pi)));
		console.log(Math.round(Math.acos(matrix.m33) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.a) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.b) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.m12) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.m13) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.m22) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.m23) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.m32) * (180/pi)));
		console.log(Math.round(Math.asin(matrix.m33) * (180/pi)));
		
		addEventListeners(window,"mousemove", get_position);	
		addEventListeners(window,"mouseup", function() {
			window.removeEventListener("mousemove", get_position);
		});
		
	});
});